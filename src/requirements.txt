Django==2.0
psycopg2==2.7.3.2
djangorestframework==3.7.7
pygments
gunicorn==19.6.0
requests==2.18.4
PyJWT
django-extensions==1.9.8
googlemaps
coreapi
shapely
stripe
google-api-python-client
raven
coverage==4.4.2
pyxero==0.9.0
django-cors-headers
celery
redis
django-celery-results
flower


